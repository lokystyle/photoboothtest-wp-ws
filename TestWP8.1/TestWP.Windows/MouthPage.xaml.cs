﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Windows.UI.Xaml.Media.Imaging;
using TestWP.Common;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkID=390556

namespace TestWP
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MouthPage : Page
    {
        public MouthPage()
        {
            this.NavigationCacheMode = NavigationCacheMode.Required;
            this.InitializeComponent();
        }

        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.
        /// This parameter is typically used to configure the page.</param>
        private void BackBtn_Click(object sender, RoutedEventArgs e)
        {
            if (Frame.CanGoBack)
                Frame.GoBack();
        }
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
           
        }

        private void Mouth1_Tapped(object sender, TappedRoutedEventArgs e)
        {
            Frame.Navigate(typeof(Page1), (BitmapImage)Mouth1.Source);
        }

        private void Mouth2_Tapped(object sender, TappedRoutedEventArgs e)
        {
            Frame.Navigate(typeof(Page1), (BitmapImage)Mouth2.Source);
        }

        private void Mouth3_Tapped(object sender, TappedRoutedEventArgs e)
        {
            Frame.Navigate(typeof(Page1), (BitmapImage)Mouth3.Source);
        }

        private void Mouth4_Tapped(object sender, TappedRoutedEventArgs e)
        {
            Frame.Navigate(typeof(Page1), (BitmapImage)Mouth4.Source);
        }

        private void Mouth5_Tapped(object sender, TappedRoutedEventArgs e)
        {
            Frame.Navigate(typeof(Page1), (BitmapImage)Mouth5.Source);
        }

        private void Mouth6_Tapped(object sender, TappedRoutedEventArgs e)
        {
            Frame.Navigate(typeof(Page1), (BitmapImage)Mouth6.Source);
        }

        private void Mouth7_Tapped(object sender, TappedRoutedEventArgs e)
        {
            Frame.Navigate(typeof(Page1), (BitmapImage)Mouth7.Source);
        }

        private void Mouth8_Tapped(object sender, TappedRoutedEventArgs e)
        {
            Frame.Navigate(typeof(Page1), (BitmapImage)Mouth8.Source);
        }

        private void Mouth9_Tapped(object sender, TappedRoutedEventArgs e)
        {
            Frame.Navigate(typeof(Page1), (BitmapImage)Mouth9.Source);
        }

        private void Mouth10_Tapped(object sender, TappedRoutedEventArgs e)
        {
            Frame.Navigate(typeof(Page1), (BitmapImage)Mouth10.Source);
        }

        private void Mouth11_Tapped(object sender, TappedRoutedEventArgs e)
        {
            Frame.Navigate(typeof(Page1), (BitmapImage)Mouth11.Source);
        }

        private void Mouth12_Tapped(object sender, TappedRoutedEventArgs e)
        {
            Frame.Navigate(typeof(Page1), (BitmapImage)Mouth12.Source);
        }

        private void Mouth13_Tapped(object sender, TappedRoutedEventArgs e)
        {
            Frame.Navigate(typeof(Page1), (BitmapImage)Mouth13.Source);
        }

        private void Mouth14_Tapped(object sender, TappedRoutedEventArgs e)
        {
            Frame.Navigate(typeof(Page1), (BitmapImage)Mouth14.Source);
        }

        private void Mouth15_Tapped(object sender, TappedRoutedEventArgs e)
        {
            Frame.Navigate(typeof(Page1), (BitmapImage)Mouth15.Source);
        }

        private void Mouth16_Tapped(object sender, TappedRoutedEventArgs e)
        {
            Frame.Navigate(typeof(Page1), (BitmapImage)Mouth16.Source);
        }

        private void Mouth17_Tapped(object sender, TappedRoutedEventArgs e)
        {
            Frame.Navigate(typeof(Page1), (BitmapImage)Mouth17.Source);
        }

        private void Mouth18_Tapped(object sender, TappedRoutedEventArgs e)
        {
            Frame.Navigate(typeof(Page1), (BitmapImage)Mouth18.Source);
        }

        private void Mouth19_Tapped(object sender, TappedRoutedEventArgs e)
        {
            Frame.Navigate(typeof(Page1), (BitmapImage)Mouth19.Source);
        }

        private void Mouth20_Tapped(object sender, TappedRoutedEventArgs e)
        {
            Frame.Navigate(typeof(Page1), (BitmapImage)Mouth20.Source);
        }

        private void Mouth21_Tapped(object sender, TappedRoutedEventArgs e)
        {
            Frame.Navigate(typeof(Page1), (BitmapImage)Mouth21.Source);
        }

        private void backBtn_Click_1(object sender, RoutedEventArgs e)
        {
            Frame.Navigate(typeof(Page1));
        }
    }
}
