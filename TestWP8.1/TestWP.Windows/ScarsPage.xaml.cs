﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Windows.UI.Xaml.Media.Imaging;
using TestWP.Common;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkID=390556

namespace TestWP
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class ScarsPage : Page
    {
        public ScarsPage()
        {
            this.NavigationCacheMode = NavigationCacheMode.Required;
            this.InitializeComponent();
        }

        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.
        /// This parameter is typically used to configure the page.</param>
        private void BackBtn_Click(object sender, RoutedEventArgs e)
        {
            if (Frame.CanGoBack)
                Frame.GoBack();
        }
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
          
        }

        private void Scars1_Tapped(object sender, TappedRoutedEventArgs e)
        {
            Frame.Navigate(typeof(Page1), (BitmapImage)Scars1.Source);
        }

        private void Scars2_Tapped(object sender, TappedRoutedEventArgs e)
        {
            Frame.Navigate(typeof(Page1), (BitmapImage)Scars2.Source);
        }

        private void Scars3_Tapped(object sender, TappedRoutedEventArgs e)
        {
            Frame.Navigate(typeof(Page1), (BitmapImage)Scars3.Source);
        }

        private void Scars4_Tapped(object sender, TappedRoutedEventArgs e)
        {
            Frame.Navigate(typeof(Page1), (BitmapImage)Scars4.Source);
        }

        private void Scars5_Tapped(object sender, TappedRoutedEventArgs e)
        {
            Frame.Navigate(typeof(Page1), (BitmapImage)Scars5.Source);
        }

        private void Scars6_Tapped(object sender, TappedRoutedEventArgs e)
        {
            Frame.Navigate(typeof(Page1), (BitmapImage)Scars6.Source);
        }

        private void Scars7_Tapped(object sender, TappedRoutedEventArgs e)
        {
            Frame.Navigate(typeof(Page1), (BitmapImage)Scars7.Source);
        }

        private void Scars8_Tapped(object sender, TappedRoutedEventArgs e)
        {
            Frame.Navigate(typeof(Page1), (BitmapImage)Scars8.Source);
        }

        private void Scars9_Tapped(object sender, TappedRoutedEventArgs e)
        {
            Frame.Navigate(typeof(Page1), (BitmapImage)Scars9.Source);
        }

        private void Scars10_Tapped(object sender, TappedRoutedEventArgs e)
        {
            Frame.Navigate(typeof(Page1), (BitmapImage)Scars10.Source);
        }

        private void Scars11_Tapped(object sender, TappedRoutedEventArgs e)
        {
            Frame.Navigate(typeof(Page1), (BitmapImage)Scars11.Source);
        }

        private void Scars12_Tapped(object sender, TappedRoutedEventArgs e)
        {
            Frame.Navigate(typeof(Page1), (BitmapImage)Scars12.Source);
        }

        private void Scars13_Tapped(object sender, TappedRoutedEventArgs e)
        {
            Frame.Navigate(typeof(Page1), (BitmapImage)Scars13.Source);
        }

        private void Scars14_Tapped(object sender, TappedRoutedEventArgs e)
        {
            Frame.Navigate(typeof(Page1), (BitmapImage)Scars14.Source);
        }

        private void Scars15_Tapped(object sender, TappedRoutedEventArgs e)
        {
            Frame.Navigate(typeof(Page1), (BitmapImage)Scars15.Source);
        }

        private void Scars16_Tapped(object sender, TappedRoutedEventArgs e)
        {
            Frame.Navigate(typeof(Page1), (BitmapImage)Scars16.Source);
        }

        private void Scars17_Tapped(object sender, TappedRoutedEventArgs e)
        {
            Frame.Navigate(typeof(Page1), (BitmapImage)Scars17.Source);
        }

        private void Scars18_Tapped(object sender, TappedRoutedEventArgs e)
        {
            Frame.Navigate(typeof(Page1), (BitmapImage)Scars18.Source);
        }

        private void Scars19_Tapped(object sender, TappedRoutedEventArgs e)
        {
            Frame.Navigate(typeof(Page1), (BitmapImage)Scars19.Source);
        }

        private void Scars20_Tapped(object sender, TappedRoutedEventArgs e)
        {
            Frame.Navigate(typeof(Page1), (BitmapImage)Scars20.Source);
        }

        private void backBtn_Click_1(object sender, RoutedEventArgs e)
        {
            Frame.Navigate(typeof(Page1));
        }

    }
}
