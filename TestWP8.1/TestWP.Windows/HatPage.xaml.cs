﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Windows.UI.Xaml.Media.Imaging;
using TestWP.Common;
// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkID=390556

namespace TestWP
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class HatPage : Page
    {
        public HatPage()
        {
            this.NavigationCacheMode = NavigationCacheMode.Required;
            this.InitializeComponent();
        }

        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.
        /// This parameter is typically used to configure the page.</param>
        private void BackBtn_Click(object sender, RoutedEventArgs e)
        {
            if (Frame.CanGoBack)
                Frame.GoBack();
        }
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
        }

        private void Hat1_Tapped(object sender, TappedRoutedEventArgs e)
        {
            Frame.Navigate(typeof(Page1), (BitmapImage)Hat1.Source);
        }

        private void Hat2_Tapped(object sender, TappedRoutedEventArgs e)
        {
            Frame.Navigate(typeof(Page1), (BitmapImage)Hat2.Source);
        }

        private void Hat3_Tapped(object sender, TappedRoutedEventArgs e)
        {
            Frame.Navigate(typeof(Page1), (BitmapImage)Hat3.Source);
        }

        private void Hat4_Tapped(object sender, TappedRoutedEventArgs e)
        {
            Frame.Navigate(typeof(Page1), (BitmapImage)Hat4.Source);
        }

        private void Hat5_Tapped(object sender, TappedRoutedEventArgs e)
        {
            Frame.Navigate(typeof(Page1), (BitmapImage)Hat5.Source);
        }

        private void Hat6_Tapped(object sender, TappedRoutedEventArgs e)
        {
            Frame.Navigate(typeof(Page1), (BitmapImage)Hat6.Source);
        }

        private void Hat7_Tapped(object sender, TappedRoutedEventArgs e)
        {
            Frame.Navigate(typeof(Page1), (BitmapImage)Hat7.Source);
        }

        private void Hat8_Tapped(object sender, TappedRoutedEventArgs e)
        {
            Frame.Navigate(typeof(Page1), (BitmapImage)Hat8.Source);
        }

        private void Hat9_Tapped(object sender, TappedRoutedEventArgs e)
        {
            Frame.Navigate(typeof(Page1), (BitmapImage)Hat9.Source);
        }

        private void Hat10_Tapped(object sender, TappedRoutedEventArgs e)
        {
            Frame.Navigate(typeof(Page1), (BitmapImage)Hat10.Source);
        }

        private void Hat11_Tapped(object sender, TappedRoutedEventArgs e)
        {
            Frame.Navigate(typeof(Page1), (BitmapImage)Hat11.Source);
        }

        private void Hat12_Tapped(object sender, TappedRoutedEventArgs e)
        {
            Frame.Navigate(typeof(Page1), (BitmapImage)Hat12.Source);
        }

        private void Hat13_Tapped(object sender, TappedRoutedEventArgs e)
        {
            Frame.Navigate(typeof(Page1), (BitmapImage)Hat13.Source);
        }

        private void Hat14_Tapped(object sender, TappedRoutedEventArgs e)
        {
            Frame.Navigate(typeof(Page1), (BitmapImage)Hat14.Source);
        }

        private void Hat15_Tapped(object sender, TappedRoutedEventArgs e)
        {
            Frame.Navigate(typeof(Page1), (BitmapImage)Hat15.Source);
        }

        private void Hat16_Tapped(object sender, TappedRoutedEventArgs e)
        {
            Frame.Navigate(typeof(Page1), (BitmapImage)Hat16.Source);
        }

        private void backBtn_Click_1(object sender, RoutedEventArgs e)
        {
            Frame.Navigate(typeof(Page1));
        }
    }
}
