﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.ApplicationModel.Activation;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Popups;
using Windows.Storage;
using Windows.Storage.Pickers;
using TestWP.Common;
using Windows.Storage.Streams;
using Windows.Graphics.Imaging;
using Windows.Graphics.Display;
using Windows.ApplicationModel.DataTransfer;
using Windows.Media.Capture;
// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=391641

namespace TestWP
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class Page1 : Page
    {
        FileOpenPicker myPicker = new FileOpenPicker();
        FileSavePicker savePicker = new FileSavePicker();
        StorageFile file;
        bool ifopenmenu = false;
        bool cansave = false;
        bool ismovable = true;
        /*private TranslateTransform move = new TranslateTransform();
        private TranslateTransform bigMove = new TranslateTransform();
        private ScaleTransform resize = new ScaleTransform();
        private RotateTransform rotation = new RotateTransform();
        private TransformGroup imageTransform = new TransformGroup();
        private TransformGroup bigimageTransform = new TransformGroup();
        RenderTargetBitmap result = new RenderTargetBitmap();
        public bool start = true;*/
        MessageDialog errSave = new MessageDialog("You need to modify a picture before saving", "Saving Error");
        private TranslateTransform move = new TranslateTransform();
        private TranslateTransform bigMove = new TranslateTransform();
        private ScaleTransform bigResize = new ScaleTransform();
        private RotateTransform bigRotation = new RotateTransform();
        private ScaleTransform resize = new ScaleTransform();
        private RotateTransform rotation = new RotateTransform();
        private TranslateTransform text_move = new TranslateTransform();
        private ScaleTransform text_resize = new ScaleTransform();
        private RotateTransform text_rotation = new RotateTransform();
        private TransformGroup imageTransform = new TransformGroup();
        private TransformGroup bigimageTransform = new TransformGroup();
        private TransformGroup textTransform = new TransformGroup();
        private TransformGroup textTransform_cur = new TransformGroup();
        RenderTargetBitmap result = new RenderTargetBitmap();
        public bool start = true;
        bool text = false;
        IBuffer pixelBuffer;
        public Page1()
        {
            this.InitializeComponent();
            myPicker.ViewMode = PickerViewMode.Thumbnail;
            myPicker.SuggestedStartLocation = PickerLocationId.PicturesLibrary;
            myPicker.FileTypeFilter.Add(".jpg");
            myPicker.FileTypeFilter.Add(".jpeg");
            myPicker.FileTypeFilter.Add(".png");
            this.NavigationCacheMode = NavigationCacheMode.Required;
            move.X = 0;
            move.Y = 0;
            resize.ScaleX = 1;
            resize.ScaleY = 1;
            rotation.Angle = 0;
            imageTransform.Children.Add(move);
            imageTransform.Children.Add(resize);
            imageTransform.Children.Add(rotation);
            textTransform_cur.Children.Add(text_move);
            textTransform_cur.Children.Add(text_resize);
            textTransform_cur.Children.Add(text_rotation);
            Adding.RenderTransform = imageTransform;
            bigimageTransform.Children.Add(bigMove);
            bigimageTransform.Children.Add(bigResize);
            bigimageTransform.Children.Add(bigRotation);
            Preview.RenderTransform = bigimageTransform;
            savePicker.DefaultFileExtension = ".png";
            savePicker.FileTypeChoices.Add(".png", new List<string> { ".png" });
            savePicker.SuggestedStartLocation = PickerLocationId.PicturesLibrary;
            savePicker.SuggestedFileName = "snapshot.png";
        }

        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.
        /// This parameter is typically used to configure the page.</param>
        BitmapImage adding = new BitmapImage();
        BitmapImage bitmapImage = new BitmapImage();
        BitmapSource temp = new BitmapImage();
        string path;



        private void OpenBtn_Click(object sender, RoutedEventArgs e)
        {
            if (!ifopenmenu)
            {
                Share_btn.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                Move_btn.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                Text_btn.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                Save_btn.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                OpenCam.Visibility = Windows.UI.Xaml.Visibility.Visible;
                OpenFileBtn.Visibility = Windows.UI.Xaml.Visibility.Visible;
                ifopenmenu = true;
            }
            else
            {
                Share_btn.Visibility = Windows.UI.Xaml.Visibility.Visible;
                Move_btn.Visibility = Windows.UI.Xaml.Visibility.Visible;
                Text_btn.Visibility = Windows.UI.Xaml.Visibility.Visible;
                Save_btn.Visibility = Windows.UI.Xaml.Visibility.Visible;
                OpenCam.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                OpenFileBtn.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                ifopenmenu = false;
            }
        }
        private void NextBtn_Click(object sender, RoutedEventArgs e)
        {
            //Frame.Navigate(typeof(Page2), bitmapImage);
        }

        private void Adding_ManipulationDelta(object sender, ManipulationDeltaRoutedEventArgs e)
        {
            rotation.CenterX = adding.PixelWidth / 2;
            rotation.CenterY = adding.PixelHeight / 2;
            resize.CenterX = adding.PixelWidth / 2;
            resize.CenterY = adding.PixelHeight / 2;
            move.X += e.Delta.Translation.X;
            move.Y += e.Delta.Translation.Y;
            if (e.Delta.Scale != 0)
            {
                resize.ScaleX *= e.Delta.Scale;
                resize.ScaleY *= e.Delta.Scale;
            }
            if (e.Delta.Rotation != 0)
            {
                rotation.Angle += e.Delta.Rotation;
            }
            e.Handled = true;
        }

        private void hat_btn_Click(object sender, RoutedEventArgs e)
        {
            Frame.Navigate(typeof(HatPage));
        }

        private void mouth_btn_Click(object sender, RoutedEventArgs e)
        {
            Frame.Navigate(typeof(MouthPage));
        }

        private void nose_btn_Click(object sender, RoutedEventArgs e)
        {
            Frame.Navigate(typeof(NosePage));
        }

        private void scars_btn_Click(object sender, RoutedEventArgs e)
        {
            Frame.Navigate(typeof(ScarsPage));
        }

        private void stuff_btn_Click(object sender, RoutedEventArgs e)
        {
            Frame.Navigate(typeof(StuffPage));
        }

        private async void Confirm_btn_Click(object sender, RoutedEventArgs e)
        {
            if (text)
            {
                TextBox.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                textBlock.Text = TextBox.Text;
                TextBox.Text = string.Empty;
                textBlock.FontSize = 40;
                textBlock.Visibility = Windows.UI.Xaml.Visibility.Visible;
                text = false;
                textBlock.ManipulationMode = ManipulationModes.All;
                resetBtn.Visibility = Windows.UI.Xaml.Visibility.Visible;
            }
            else
            {
                textTransform = textTransform_cur;
                textBlock.ManipulationMode = ManipulationModes.None;
                await result.RenderAsync(RenderFrame);
                Adding.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                textBlock.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                Preview.Source = result;
                Share_btn.Visibility = Windows.UI.Xaml.Visibility.Visible;
                Save_btn.Visibility = Windows.UI.Xaml.Visibility.Visible;
                OpenBtn.Visibility = Windows.UI.Xaml.Visibility.Visible;
                hat_btn.Visibility = Windows.UI.Xaml.Visibility.Visible;
                mouth_btn.Visibility = Windows.UI.Xaml.Visibility.Visible;
                nose_btn.Visibility = Windows.UI.Xaml.Visibility.Visible;
                scars_btn.Visibility = Windows.UI.Xaml.Visibility.Visible;
                stuff_btn.Visibility = Windows.UI.Xaml.Visibility.Visible;
                Move_btn.Visibility = Windows.UI.Xaml.Visibility.Visible;
                Text_btn.Visibility = Windows.UI.Xaml.Visibility.Visible;
                Confirm_btn.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                Cancel_btn.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                resetBtn.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                //Frame.BackStack.RemoveAt(Frame.BackStackDepth - 1);
                //canshare = false;
                cansave = true;
                //Save_btn.IsTapEnabled = true;
                /* pixelWidth = result.PixelWidth;
                 pixelHight = result.PixelHeight;
                 pixelBuffer = await result.GetPixelsAsync();*/
            }
        }

        private void Cancel_btn_Click(object sender, RoutedEventArgs e)
        {
            textTransform_cur = textTransform;
            TextBox.Text = string.Empty;
            textBlock.ManipulationMode = ManipulationModes.None;
            TextBox.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            textBlock.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            Adding.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            Share_btn.Visibility = Windows.UI.Xaml.Visibility.Visible;
            Save_btn.Visibility = Windows.UI.Xaml.Visibility.Visible;
            hat_btn.Visibility = Windows.UI.Xaml.Visibility.Visible;
            mouth_btn.Visibility = Windows.UI.Xaml.Visibility.Visible;
            nose_btn.Visibility = Windows.UI.Xaml.Visibility.Visible;
            scars_btn.Visibility = Windows.UI.Xaml.Visibility.Visible;
            stuff_btn.Visibility = Windows.UI.Xaml.Visibility.Visible;
            OpenBtn.Visibility = Windows.UI.Xaml.Visibility.Visible;
            Move_btn.Visibility = Windows.UI.Xaml.Visibility.Visible;
            Text_btn.Visibility = Windows.UI.Xaml.Visibility.Visible;
            Confirm_btn.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            Cancel_btn.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            resetBtn.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            Frame.BackStack.RemoveAt(Frame.BackStackDepth - 1);
        }
        private void RegisterForShare()
        {
            DataTransferManager dataTransferManager = DataTransferManager.GetForCurrentView();
            dataTransferManager.DataRequested += new TypedEventHandler<DataTransferManager, DataRequestedEventArgs>(this.MainPage_DataRequested);
        }
        private async void MainPage_DataRequested(DataTransferManager sender, DataRequestedEventArgs args)
        {
            var deferral = args.Request.GetDeferral();
            var bitmap = new RenderTargetBitmap();
            await bitmap.RenderAsync(RenderFrame);

            // 1. Get the pixels
            IBuffer pixelBuffer = await bitmap.GetPixelsAsync();
            byte[] pixels = pixelBuffer.ToArray();

            // 2. Write the pixels to a InMemoryRandomAccessStream
            var stream = new InMemoryRandomAccessStream();
            var encoder = await BitmapEncoder.CreateAsync(BitmapEncoder.BmpEncoderId, stream);

            encoder.SetPixelData(BitmapPixelFormat.Bgra8, BitmapAlphaMode.Straight, (uint)bitmap.PixelWidth, (uint)bitmap.PixelHeight, 96, 96,
                pixels);

            await encoder.FlushAsync();
            stream.Seek(0);

            // 3. Share it
            args.Request.Data.SetBitmap(RandomAccessStreamReference.CreateFromStream(stream));
            //args.Request.Data.Properties.Description = "description";
            args.Request.Data.Properties.Title = "snapshot.png";
            deferral.Complete();
        }
        private void Share_btn_Click(object sender, RoutedEventArgs e)
        {

            RegisterForShare();
            DataTransferManager.ShowShareUI();
        }

        private async void Save_btn_Click(object sender, RoutedEventArgs e)
        {
            if (cansave)
            {
                pixelBuffer = await result.GetPixelsAsync();
                RenderTargetBitmap renderTargetBitmap = new RenderTargetBitmap();
                await renderTargetBitmap.RenderAsync(RenderFrame);

                var savePicker = new FileSavePicker();
                savePicker.DefaultFileExtension = ".png";
                savePicker.FileTypeChoices.Add(".png", new List<string> { ".png" });
                savePicker.SuggestedStartLocation = PickerLocationId.PicturesLibrary;
                savePicker.SuggestedFileName = "snapshot.png";

                var saveFile = await savePicker.PickSaveFileAsync();

                // Verify the user selected a file
                if (saveFile == null)
                    return;

                // Encode the image to the selected file on disk
                using (var fileStream = await saveFile.OpenAsync(FileAccessMode.ReadWrite))
                {
                    var encoder = await BitmapEncoder.CreateAsync(BitmapEncoder.PngEncoderId, fileStream);

                    encoder.SetPixelData(
                        BitmapPixelFormat.Bgra8,
                        BitmapAlphaMode.Ignore,
                        (uint)renderTargetBitmap.PixelWidth,
                        (uint)renderTargetBitmap.PixelHeight,
                        DisplayInformation.GetForCurrentView().LogicalDpi,
                        DisplayInformation.GetForCurrentView().LogicalDpi,
                        pixelBuffer.ToArray());

                    await encoder.FlushAsync();
                }
            }
            else
                await errSave.ShowAsync();
        }
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            if (e.Parameter != null)
            {
                adding = (BitmapImage)e.Parameter;
                Adding.Source = adding;
                Adding.Visibility = Windows.UI.Xaml.Visibility.Visible;
                hat_btn.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                mouth_btn.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                nose_btn.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                scars_btn.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                stuff_btn.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                Confirm_btn.Visibility = Windows.UI.Xaml.Visibility.Visible;
                Cancel_btn.Visibility = Windows.UI.Xaml.Visibility.Visible;
                resetBtn.Visibility = Windows.UI.Xaml.Visibility.Visible;
                Move_btn.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                Text_btn.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                Save_btn.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                Share_btn.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                OpenBtn.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                //DataTransferManager.GetForCurrentView().DataRequested += OnShareDataRequested;
            }
            /*else if (!start)
            {
                Adding.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                hat_btn.Visibility = Windows.UI.Xaml.Visibility.Visible;
                mouth_btn.Visibility = Windows.UI.Xaml.Visibility.Visible;
                nose_btn.Visibility = Windows.UI.Xaml.Visibility.Visible;
                scars_btn.Visibility = Windows.UI.Xaml.Visibility.Visible;
                stuff_btn.Visibility = Windows.UI.Xaml.Visibility.Visible;
                Confirm_btn.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                Cancel_btn.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                Save_btn.Visibility = Windows.UI.Xaml.Visibility.Visible;
                Share_btn.Visibility = Windows.UI.Xaml.Visibility.Visible;
                OpenBtn.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            }*/
            else
            {
                OpenBtn.Visibility = Windows.UI.Xaml.Visibility.Visible;
                start = false;
            }
        }


        private async void OpenCam_Click(object sender, RoutedEventArgs e)
        {
            CameraCaptureUI dialog = new CameraCaptureUI();
            Size aspectRatio = new Size(9, 16);
            dialog.PhotoSettings.CroppedAspectRatio = aspectRatio;

            StorageFile file = await dialog.CaptureFileAsync(CameraCaptureUIMode.Photo);
            if (file != null)
            {
                using (IRandomAccessStream fileStream = await file.OpenAsync(Windows.Storage.FileAccessMode.Read))
                {
                    // Set the image source to the selected bitmap 
                    await bitmapImage.SetSourceAsync(fileStream);
                    Preview.Source = bitmapImage;
                    cansave = false;
                }
            }
            Share_btn.Visibility = Windows.UI.Xaml.Visibility.Visible;
            Move_btn.Visibility = Windows.UI.Xaml.Visibility.Visible;
            Text_btn.Visibility = Windows.UI.Xaml.Visibility.Visible;
            Save_btn.Visibility = Windows.UI.Xaml.Visibility.Visible;
            OpenCam.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            OpenFileBtn.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            ifopenmenu = false;
        }

        private void Move_btn_Click(object sender, RoutedEventArgs e)
        {
            if (ismovable)
            {
                Move_btn.Background.Opacity = 0.7;
                Preview.ManipulationMode = ManipulationModes.All;
                hat_btn.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                mouth_btn.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                nose_btn.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                scars_btn.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                stuff_btn.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                resetBtn.Visibility = Windows.UI.Xaml.Visibility.Visible;
                ismovable = false;
            }
            else
            {
                Move_btn.Background.Opacity = 1;
                Preview.ManipulationMode = ManipulationModes.None;
                hat_btn.Visibility = Windows.UI.Xaml.Visibility.Visible;
                mouth_btn.Visibility = Windows.UI.Xaml.Visibility.Visible;
                nose_btn.Visibility = Windows.UI.Xaml.Visibility.Visible;
                scars_btn.Visibility = Windows.UI.Xaml.Visibility.Visible;
                stuff_btn.Visibility = Windows.UI.Xaml.Visibility.Visible;
                resetBtn.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                ismovable = true;
            }
        }

        private void Preview_ManipulationDelta(object sender, ManipulationDeltaRoutedEventArgs e)
        {
            //rotation.CenterX =  Preview.ActualWidth/ 2;
            //rotation.CenterY = Preview.ActualHeight / 2;
            resize.CenterX = Preview.ActualWidth / 2;
            resize.CenterY = Preview.ActualHeight / 2;
            bigMove.X += e.Delta.Translation.X;
            bigMove.Y += e.Delta.Translation.Y;
            if (e.Delta.Scale != 0)
            {
                resize.ScaleX *= e.Delta.Scale;
                resize.ScaleY *= e.Delta.Scale;
            }
            //if (e.Delta.Rotation != 0)
            //{
              //  rotation.Angle += e.Delta.Rotation;
            //}
            e.Handled = true;
        }

        private void resetBtn_Click(object sender, RoutedEventArgs e)
        {
            if (!text)
            {
                text_move.X = 0;
                text_move.Y = 0;
            }
            if (!ismovable)
            {
                bigMove.X = 0;
                bigMove.Y = 0;
            }
            else
            {
                move.X = 0;
                move.Y = 0;
            }
        }

        private void Text_btn_Click(object sender, RoutedEventArgs e)
        {
            text = true;
            textBlock.RenderTransform = textTransform_cur;
            text_move.X = 0;
            text_move.Y = 0;
            text_rotation.Angle = 0;
            text_resize.ScaleX = 1;
            text_resize.ScaleY = 1;
            hat_btn.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            mouth_btn.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            nose_btn.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            scars_btn.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            stuff_btn.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            Confirm_btn.Visibility = Windows.UI.Xaml.Visibility.Visible;
            Cancel_btn.Visibility = Windows.UI.Xaml.Visibility.Visible;
            Save_btn.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            Share_btn.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            OpenBtn.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            Text_btn.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            Move_btn.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            TextBox.Visibility = Windows.UI.Xaml.Visibility.Visible;
        }

        private async void OpenFileBtn_Click(object sender, RoutedEventArgs e)
        {
            FileOpenPicker myPicker = new FileOpenPicker();
            myPicker.ViewMode = PickerViewMode.Thumbnail;
            myPicker.SuggestedStartLocation = PickerLocationId.PicturesLibrary;
            myPicker.FileTypeFilter.Add(".jpg");
            myPicker.FileTypeFilter.Add(".jpeg");
            myPicker.FileTypeFilter.Add(".png");
            StorageFile file = await myPicker.PickSingleFileAsync();
            if (file != null)
            {
                using (IRandomAccessStream fileStream = await file.OpenAsync(Windows.Storage.FileAccessMode.Read))
                {
                    // Set the image source to the selected bitmap 
                    await bitmapImage.SetSourceAsync(fileStream);
                    Preview.Source = bitmapImage;
                    ifopenmenu = false;
                }
                
            }
            else if (Preview.Source != null)
            {
                MessageDialog message = new MessageDialog("Please select image file", "Image file not selected");
                await message.ShowAsync();
            }
            Share_btn.Visibility = Windows.UI.Xaml.Visibility.Visible;
            Move_btn.Visibility = Windows.UI.Xaml.Visibility.Visible;
            Text_btn.Visibility = Windows.UI.Xaml.Visibility.Visible;
            Save_btn.Visibility = Windows.UI.Xaml.Visibility.Visible;
            OpenCam.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            OpenFileBtn.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            
            
        }

        private void textBlock_ManipulationDelta(object sender, ManipulationDeltaRoutedEventArgs e)
        {
            text_rotation.CenterX = textBlock.ActualWidth / 2;
            text_rotation.CenterY = textBlock.ActualHeight / 2;
            text_resize.CenterX = textBlock.ActualWidth / 2;
            text_resize.CenterY = textBlock.ActualHeight / 2;
            text_move.X += e.Delta.Translation.X;
            text_move.Y += e.Delta.Translation.Y;
            if (e.Delta.Scale != 0)
            {
                text_resize.ScaleX *= e.Delta.Scale;
                text_resize.ScaleY *= e.Delta.Scale;
            }
            if (e.Delta.Rotation != 0)
            {
                text_rotation.Angle += e.Delta.Rotation;
            }
        }

       


    }
}

