﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Windows.UI.Xaml.Media.Imaging;
using TestWP.Common;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkID=390556

namespace TestWP
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class StuffPage : Page
    {
        BitmapImage temp = new BitmapImage();
        public StuffPage()
        {
            this.NavigationCacheMode = NavigationCacheMode.Required;
            this.InitializeComponent();
            Windows.Phone.UI.Input.HardwareButtons.BackPressed += HardwareButtons_BackPressed;
        }

        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.
        /// This parameter is typically used to configure the page.</param>
        void HardwareButtons_BackPressed(object sender, Windows.Phone.UI.Input.BackPressedEventArgs e)
        {
            Frame.Navigate(typeof(Page1));
            e.Handled = true;
        }
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {

        }

        private void Stuff1_Tapped(object sender, TappedRoutedEventArgs e)
        {
            temp = (BitmapImage)Stuff1.Source;
            string path = temp.UriSource.ToString();
            Frame.Navigate(typeof(Page1), path);
        }

        private void Stuff2_Tapped(object sender, TappedRoutedEventArgs e)
        {
            temp = (BitmapImage)Stuff2.Source;
            string path = temp.UriSource.ToString();
            Frame.Navigate(typeof(Page1), path);
        }

        private void Stuff3_Tapped(object sender, TappedRoutedEventArgs e)
        {
            temp = (BitmapImage)Stuff3.Source;
            string path = temp.UriSource.ToString();
            Frame.Navigate(typeof(Page1), path);
        }

        private void Stuff4_Tapped(object sender, TappedRoutedEventArgs e)
        {
            temp = (BitmapImage)Stuff4.Source;
            string path = temp.UriSource.ToString();
            Frame.Navigate(typeof(Page1), path);
        }

        private void Stuff5_Tapped(object sender, TappedRoutedEventArgs e)
        {
            temp = (BitmapImage)Stuff5.Source;
            string path = temp.UriSource.ToString();
            Frame.Navigate(typeof(Page1), path);
        }

        private void Stuff6_Tapped(object sender, TappedRoutedEventArgs e)
        {
            temp = (BitmapImage)Stuff6.Source;
            string path = temp.UriSource.ToString();
            Frame.Navigate(typeof(Page1), path);
        }

        private void Stuff7_Tapped(object sender, TappedRoutedEventArgs e)
        {
            temp = (BitmapImage)Stuff7.Source;
            string path = temp.UriSource.ToString();
            Frame.Navigate(typeof(Page1), path);
        }

        private void Stuff8_Tapped(object sender, TappedRoutedEventArgs e)
        {
            temp = (BitmapImage)Stuff8.Source;
            string path = temp.UriSource.ToString();
            Frame.Navigate(typeof(Page1), path);
        }

        private void Stuff9_Tapped(object sender, TappedRoutedEventArgs e)
        {
            temp = (BitmapImage)Stuff9.Source;
            string path = temp.UriSource.ToString();
            Frame.Navigate(typeof(Page1), path);
        }

        private void Stuff10_Tapped(object sender, TappedRoutedEventArgs e)
        {
            temp = (BitmapImage)Stuff10.Source;
            string path = temp.UriSource.ToString();
            Frame.Navigate(typeof(Page1), path);
        }

        private void Stuff11_Tapped(object sender, TappedRoutedEventArgs e)
        {
            temp = (BitmapImage)Stuff11.Source;
            string path = temp.UriSource.ToString();
            Frame.Navigate(typeof(Page1), path);
        }

        private void Stuff12_Tapped(object sender, TappedRoutedEventArgs e)
        {
            temp = (BitmapImage)Stuff12.Source;
            string path = temp.UriSource.ToString();
            Frame.Navigate(typeof(Page1), path);
        }

        private void Stuff13_Tapped(object sender, TappedRoutedEventArgs e)
        {
            temp = (BitmapImage)Stuff13.Source;
            string path = temp.UriSource.ToString();
            Frame.Navigate(typeof(Page1), path);
        }

        private void Stuff14_Tapped(object sender, TappedRoutedEventArgs e)
        {
            temp = (BitmapImage)Stuff14.Source;
            string path = temp.UriSource.ToString();
            Frame.Navigate(typeof(Page1), path);
        }

        private void Stuff15_Tapped(object sender, TappedRoutedEventArgs e)
        {
            temp = (BitmapImage)Stuff15.Source;
            string path = temp.UriSource.ToString();
            Frame.Navigate(typeof(Page1), path);
        }

        private void Stuff16_Tapped(object sender, TappedRoutedEventArgs e)
        {
            temp = (BitmapImage)Stuff16.Source;
            string path = temp.UriSource.ToString();
            Frame.Navigate(typeof(Page1), path);
        }

        private void Stuff17_Tapped(object sender, TappedRoutedEventArgs e)
        {
            temp = (BitmapImage)Stuff17.Source;
            string path = temp.UriSource.ToString();
            Frame.Navigate(typeof(Page1), path);
        }

        private void Stuff18_Tapped(object sender, TappedRoutedEventArgs e)
        {
            temp = (BitmapImage)Stuff18.Source;
            string path = temp.UriSource.ToString();
            Frame.Navigate(typeof(Page1), path);
        }
    }
}
