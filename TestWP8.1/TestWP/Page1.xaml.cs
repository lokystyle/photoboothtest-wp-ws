﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Windows;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.ApplicationModel.Activation;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Popups;
using Windows.Storage;
using Windows.Storage.Pickers;
using TestWP.Common;
using Windows.Storage.Streams;
using Windows.Graphics.Imaging;
using Windows.Graphics.Display;
using Windows.ApplicationModel.DataTransfer;
// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=391641

namespace TestWP
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class Page1 : Page, IFileOpenPickerContinuable, IFileSavePickerContinuable
    {
        FileOpenPicker myPicker = new FileOpenPicker();
        FileSavePicker savePicker = new FileSavePicker();
        MessageDialog err = new MessageDialog("You need to save a picture before sharing", "Sharing Error");
        MessageDialog errSave = new MessageDialog("You need to modify a picture before saving", "Saving Error");
        StorageFile file, savefile;
        bool canshare = false;
        bool ismovable = true;
        bool cansave = false;
        private TranslateTransform move = new TranslateTransform();
        private TranslateTransform bigMove = new TranslateTransform();
        private TranslateTransform test = new TranslateTransform();
        private ScaleTransform bigResize = new ScaleTransform();
        private RotateTransform bigRotation = new RotateTransform();
        private ScaleTransform resize = new ScaleTransform();
        private RotateTransform rotation = new RotateTransform();
        private TranslateTransform text_move = new TranslateTransform();
        private ScaleTransform text_resize = new ScaleTransform();
        private RotateTransform text_rotation = new RotateTransform();
        private TransformGroup imageTransform = new TransformGroup();
        private TransformGroup bigimageTransform = new TransformGroup();
        private TransformGroup textTransform = new TransformGroup();
        private TransformGroup textTransform_cur = new TransformGroup();

       
        RenderTargetBitmap result = new RenderTargetBitmap();
        public bool start = true;
        bool text = false;
        IBuffer pixelBuffer;
        public Page1()
        {
            this.InitializeComponent();
            myPicker.ViewMode = PickerViewMode.Thumbnail;
            myPicker.SuggestedStartLocation = PickerLocationId.PicturesLibrary;
            myPicker.FileTypeFilter.Add(".jpg");
            myPicker.FileTypeFilter.Add(".jpeg");
            myPicker.FileTypeFilter.Add(".png");
            this.NavigationCacheMode = NavigationCacheMode.Required;
            move.X = 0;
            move.Y = 0;
            resize.ScaleX = 1;
            resize.ScaleY = 1;
            rotation.Angle = 0;
            imageTransform.Children.Add(move);
            imageTransform.Children.Add(resize);
            imageTransform.Children.Add(rotation);
            textTransform_cur.Children.Add(text_move);
            textTransform_cur.Children.Add(text_resize);
            textTransform_cur.Children.Add(text_rotation);
            Adding.RenderTransform = imageTransform;
            bigimageTransform.Children.Add(bigMove);
            bigimageTransform.Children.Add(bigResize);
            bigimageTransform.Children.Add(bigRotation);
            Preview.RenderTransform = bigimageTransform;
            OpenBtn.Visibility = Windows.UI.Xaml.Visibility.Visible;
            savePicker.DefaultFileExtension = ".png";
            savePicker.FileTypeChoices.Add(".png", new List<string> { ".png" });
            savePicker.SuggestedStartLocation = PickerLocationId.PicturesLibrary;
            savePicker.SuggestedFileName = "snapshot.png";
            Windows.Phone.UI.Input.HardwareButtons.BackPressed += HardwareButtons_BackPressed;
        }

        

        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.
        /// This parameter is typically used to configure the page.</param>
        BitmapImage adding = new BitmapImage();
        BitmapImage bitmapImage = new BitmapImage();
        BitmapSource temp = new BitmapImage();
        string path;
        void HardwareButtons_BackPressed(object sender, Windows.Phone.UI.Input.BackPressedEventArgs e)
        {
            if (!e.Handled && Frame.CurrentSourcePageType.FullName == "TestWP.MainPage")
                Application.Current.Exit();
            else
            if (Cancel_btn.Visibility == Windows.UI.Xaml.Visibility.Visible)
            {
                textTransform_cur = textTransform;
                TextBox.Text = string.Empty;
                textBlock.ManipulationMode = ManipulationModes.None;
                TextBox.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                textBlock.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                Adding.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                Share_btn.Visibility = Windows.UI.Xaml.Visibility.Visible;
                Save_btn.Visibility = Windows.UI.Xaml.Visibility.Visible;
                hat_btn.Visibility = Windows.UI.Xaml.Visibility.Visible;
                mouth_btn.Visibility = Windows.UI.Xaml.Visibility.Visible;
                nose_btn.Visibility = Windows.UI.Xaml.Visibility.Visible;
                scars_btn.Visibility = Windows.UI.Xaml.Visibility.Visible;
                stuff_btn.Visibility = Windows.UI.Xaml.Visibility.Visible;
                OpenBtn.Visibility = Windows.UI.Xaml.Visibility.Visible;
                Move_btn.Visibility = Windows.UI.Xaml.Visibility.Visible;
                Text_btn.Visibility = Windows.UI.Xaml.Visibility.Visible;
                Confirm_btn.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                Cancel_btn.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                resetBtn.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                e.Handled = true;
            }
            
        }
        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            base.OnNavigatedFrom(e);
           // Frame.BackStack.RemoveAt(Frame.BackStackDepth - 1);
        }
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            if (e.Parameter != null)
            {
                adding = new BitmapImage(new Uri((string)e.Parameter));
                Frame.BackStack.RemoveAt(Frame.BackStackDepth - 1);
                Adding.Source = adding;
                Adding.Visibility = Windows.UI.Xaml.Visibility.Visible;
                hat_btn.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                mouth_btn.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                nose_btn.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                scars_btn.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                stuff_btn.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                Confirm_btn.Visibility = Windows.UI.Xaml.Visibility.Visible;
                Cancel_btn.Visibility = Windows.UI.Xaml.Visibility.Visible;
                resetBtn.Visibility = Windows.UI.Xaml.Visibility.Visible;
                Move_btn.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                Text_btn.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                Save_btn.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                Share_btn.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                OpenBtn.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                DataTransferManager.GetForCurrentView().DataRequested += OnShareDataRequested;
            }
            /*else if (!start)
            {
                Adding.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                hat_btn.Visibility = Windows.UI.Xaml.Visibility.Visible;
                mouth_btn.Visibility = Windows.UI.Xaml.Visibility.Visible;
                nose_btn.Visibility = Windows.UI.Xaml.Visibility.Visible;
                scars_btn.Visibility = Windows.UI.Xaml.Visibility.Visible;
                stuff_btn.Visibility = Windows.UI.Xaml.Visibility.Visible;
                Confirm_btn.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                Cancel_btn.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                Save_btn.Visibility = Windows.UI.Xaml.Visibility.Visible;
                Share_btn.Visibility = Windows.UI.Xaml.Visibility.Visible;
                OpenBtn.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            }*/
            else
            {
                OpenBtn.Visibility = Windows.UI.Xaml.Visibility.Visible;
                start = false;
            }
        }

        private void OpenBtn_Click(object sender, RoutedEventArgs e)
        {
            cansave = false;
            myPicker.PickSingleFileAndContinue();          
        }

        public async void ContinueFileOpenPicker(FileOpenPickerContinuationEventArgs args)
        {
            Save_btn.IsTapEnabled = false;
            if (args.Files.Count > 0)
            {
                file = args.Files[0];
              path = args.Files[0].Path;
               
               using (IRandomAccessStream fileStream = await args.Files[0].OpenAsync(Windows.Storage.FileAccessMode.Read))
                {
                    // Set the image source to the selected bitmap 
                    await bitmapImage.SetSourceAsync(fileStream);
                    Preview.Source = bitmapImage;
                }
               await result.RenderAsync(RenderFrame);
            }
            else if (Preview.Source == null)
            {
                MessageDialog message = new MessageDialog("Please select image file", "Image file not selected");
                await message.ShowAsync();
            }
        }

        private void hat_btn_Click(object sender, RoutedEventArgs e)
        {
            Frame.Navigate(typeof(HatPage));
        }

        private void mouth_btn_Click(object sender, RoutedEventArgs e)
        {
            Frame.Navigate(typeof(MouthPage));
        }

        private void nose_btn_Click(object sender, RoutedEventArgs e)
        {
            Frame.Navigate(typeof(NosePage));
        }

        private void scars_btn_Click(object sender, RoutedEventArgs e)
        {
            Frame.Navigate(typeof(ScarsPage));
        }

        private void stuff_btn_Click(object sender, RoutedEventArgs e)
        {
            Frame.Navigate(typeof(StuffPage));
        }

        private void Adding_ManipulationDelta(object sender, ManipulationDeltaRoutedEventArgs e)
        {
                /*rotation.CenterX = adding.PixelWidth / 2;
                rotation.CenterY = adding.PixelHeight / 2;
                resize.CenterX = adding.PixelWidth / 2;
                resize.CenterY = adding.PixelHeight / 2;*/
                if (e.Delta.Scale != 0)
                {
                    resize.ScaleX *= e.Delta.Scale;
                    resize.ScaleY *= e.Delta.Scale;
                }
                if (e.Delta.Rotation != 0)
                {
                    if (rotation.Angle == 360 || rotation.Angle == -360)
                        rotation.Angle = 0;
                  rotation.Angle += e.Delta.Rotation;
                   // rotation.Angle = 90;
                }
                move.X += Math.Cos(rotation.Angle * Math.PI / 180) * e.Delta.Translation.X + Math.Sin(rotation.Angle * Math.PI / 180) * e.Delta.Translation.Y;
                move.Y += Math.Cos(rotation.Angle * Math.PI / 180) * e.Delta.Translation.Y - Math.Sin(rotation.Angle * Math.PI / 180) * e.Delta.Translation.X;
                e.Handled = true;
        }
        
        private async void Confirm_btn_Click(object sender, RoutedEventArgs e)
        {
            if (text)
            {
                TextBox.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                textBlock.Text = TextBox.Text;
                TextBox.Text = string.Empty;
                textBlock.FontSize = 40;
                textBlock.Visibility = Windows.UI.Xaml.Visibility.Visible;
                text = false;
                textBlock.ManipulationMode = ManipulationModes.All;
                resetBtn.Visibility = Windows.UI.Xaml.Visibility.Visible;
            }
            else
            {
                textTransform = textTransform_cur;
                textBlock.ManipulationMode = ManipulationModes.None;
                await result.RenderAsync(RenderFrame);
                Adding.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                rotation.Angle = 0;
                textBlock.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                Preview.Source = result;
                Share_btn.Visibility = Windows.UI.Xaml.Visibility.Visible;
                Save_btn.Visibility = Windows.UI.Xaml.Visibility.Visible;
                OpenBtn.Visibility = Windows.UI.Xaml.Visibility.Visible;
                hat_btn.Visibility = Windows.UI.Xaml.Visibility.Visible;
                mouth_btn.Visibility = Windows.UI.Xaml.Visibility.Visible;
                nose_btn.Visibility = Windows.UI.Xaml.Visibility.Visible;
                scars_btn.Visibility = Windows.UI.Xaml.Visibility.Visible;
                stuff_btn.Visibility = Windows.UI.Xaml.Visibility.Visible;
                Move_btn.Visibility = Windows.UI.Xaml.Visibility.Visible;
                Text_btn.Visibility = Windows.UI.Xaml.Visibility.Visible;
                Confirm_btn.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                Cancel_btn.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                resetBtn.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                
                canshare = false;
                cansave = true;
                Save_btn.IsTapEnabled = true;
                /* pixelWidth = result.PixelWidth;
                 pixelHight = result.PixelHeight;
                 pixelBuffer = await result.GetPixelsAsync();*/
            }
        }

        private void Cancel_btn_Click(object sender, RoutedEventArgs e)
        {
            textTransform_cur = textTransform;
            TextBox.Text = string.Empty;
            textBlock.ManipulationMode = ManipulationModes.None;
            resize.ScaleX = 1;
            resize.ScaleY = 1;
            TextBox.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            textBlock.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            Adding.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            Share_btn.Visibility = Windows.UI.Xaml.Visibility.Visible;
            Save_btn.Visibility = Windows.UI.Xaml.Visibility.Visible;
            hat_btn.Visibility = Windows.UI.Xaml.Visibility.Visible;
            mouth_btn.Visibility = Windows.UI.Xaml.Visibility.Visible;
            nose_btn.Visibility = Windows.UI.Xaml.Visibility.Visible;
            scars_btn.Visibility = Windows.UI.Xaml.Visibility.Visible;
            stuff_btn.Visibility = Windows.UI.Xaml.Visibility.Visible;
            OpenBtn.Visibility = Windows.UI.Xaml.Visibility.Visible;
            Move_btn.Visibility = Windows.UI.Xaml.Visibility.Visible;
            Text_btn.Visibility = Windows.UI.Xaml.Visibility.Visible;
            Confirm_btn.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            Cancel_btn.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            resetBtn.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            //Frame.BackStack.RemoveAt(Frame.BackStackDepth - 1);
        }
         void OnShareDataRequested(DataTransferManager sender, DataRequestedEventArgs args)
        {
            var dp = args.Request.Data;
            var deferral = args.Request.GetDeferral();

            dp.Properties.Title = "snapshot.png";
            dp.SetStorageItems(new List<StorageFile> {savefile});
            deferral.Complete();
        }
        private async void Share_btn_Click(object sender, RoutedEventArgs e)
        {
            if (ismovable)
            {
                if (canshare)
                    DataTransferManager.ShowShareUI();
                else
                {
                    err.Content = "You need to save a picture before sharing";
                    err.Title = "Sharing error";
                    await err.ShowAsync();
                }
            }
        }

        public async void ContinueFileSavePicker(FileSavePickerContinuationEventArgs args)
        {
            savefile = args.File;
            if (savefile != null)
            {
                using (var fileStream = await savefile.OpenAsync(FileAccessMode.ReadWrite))
                {
                    var encoder = await BitmapEncoder.CreateAsync(BitmapEncoder.PngEncoderId, fileStream);

                    encoder.SetPixelData(
                        BitmapPixelFormat.Bgra8,
                        BitmapAlphaMode.Straight,
                        (uint)result.PixelWidth,
                        (uint)result.PixelHeight,
                        DisplayInformation.GetForCurrentView().LogicalDpi,
                        DisplayInformation.GetForCurrentView().LogicalDpi,
                        pixelBuffer.ToArray());
                    canshare = true;
                    await encoder.FlushAsync();
                }
            }
        }

        private void Move_btn_Click(object sender, RoutedEventArgs e)
        {
            if (ismovable)
            {
                Move_btn.Background.Opacity = 0.7;
                Preview.ManipulationMode = ManipulationModes.All;
                hat_btn.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                mouth_btn.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                scars_btn.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                stuff_btn.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                resetBtn.Visibility = Windows.UI.Xaml.Visibility.Visible;
                ismovable = false;
            }
            else
            {
                Move_btn.Background.Opacity = 1;
                Preview.ManipulationMode = ManipulationModes.None;
                hat_btn.Visibility = Windows.UI.Xaml.Visibility.Visible;
                mouth_btn.Visibility = Windows.UI.Xaml.Visibility.Visible;
                scars_btn.Visibility = Windows.UI.Xaml.Visibility.Visible;
                stuff_btn.Visibility = Windows.UI.Xaml.Visibility.Visible;
                resetBtn.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                ismovable = true;
            }

        
        }

        private void Text_btn_Click(object sender, RoutedEventArgs e)
        {
            if (ismovable)
            {
                text = true;
                textBlock.RenderTransform = textTransform_cur;
                text_move.X = 1 / resize.ScaleX;
                text_move.Y = 1 / resize.ScaleY;
                text_rotation.Angle = 0;
                text_resize.ScaleX = 1;
                text_resize.ScaleY = 1;
                hat_btn.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                mouth_btn.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                nose_btn.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                scars_btn.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                stuff_btn.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                Confirm_btn.Visibility = Windows.UI.Xaml.Visibility.Visible;
                Cancel_btn.Visibility = Windows.UI.Xaml.Visibility.Visible;
                Save_btn.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                Share_btn.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                OpenBtn.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                Text_btn.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                Move_btn.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                TextBox.Visibility = Windows.UI.Xaml.Visibility.Visible;
            }
        }

        private void Preview_ManipulationDelta(object sender, ManipulationDeltaRoutedEventArgs e)
        {
            bigRotation.CenterX =  Preview.ActualWidth/ 2;
            bigRotation.CenterY = Preview.ActualHeight / 2;
            bigResize.CenterX = Preview.ActualWidth / 2;
            bigResize.CenterY = Preview.ActualHeight / 2;
            bigMove.X += e.Delta.Translation.X;
            bigMove.Y += e.Delta.Translation.Y;
            if (e.Delta.Scale != 0)
            {
                bigResize.ScaleX *= e.Delta.Scale;
                bigResize.ScaleY *= e.Delta.Scale;
            }
           /* if (e.Delta.Rotation != 0)
            {
                bigRotation.Angle += e.Delta.Rotation;
            }*/
            e.Handled = true;
        }

        private void resetBtn_Click(object sender, RoutedEventArgs e)
        {
            if (!text)
            {
                text_move.X = 0;
                text_move.Y = 0;
            }
            if (!ismovable)
            {
                bigMove.X = 0;
                bigMove.Y = 0;
            }
            else
            {
                if (resize.ScaleX == 1 && resize.ScaleY == 1)
                {
                    move.X = 0;
                    move.Y = 0;
                }
                else
                {
                    move.X = 0;
                    move.Y = 0;
                }
            }
        }

        private void textBlock_ManipulationDelta(object sender, ManipulationDeltaRoutedEventArgs e)
        {
                text_rotation.CenterX = textBlock.ActualWidth / 2;
                text_rotation.CenterY = textBlock.ActualHeight / 2;
                text_resize.CenterX = textBlock.ActualWidth / 2;
                text_resize.CenterY = textBlock.ActualHeight / 2;
                text_move.X += e.Delta.Translation.X;
                text_move.Y += e.Delta.Translation.Y;
                if (e.Delta.Scale != 0)
                {
                    text_resize.ScaleX *= e.Delta.Scale;
                    text_resize.ScaleY *= e.Delta.Scale;
                }
                if (e.Delta.Rotation != 0)
                {
                    text_rotation.Angle += e.Delta.Rotation;
                }
        }

        private void RenderFrame_ManipulationDelta(object sender, ManipulationDeltaRoutedEventArgs e)
        {

        }

        private async void Save_btn_Tapped(object sender, TappedRoutedEventArgs e)
        {
            if (ismovable)
            {
                if (cansave)
                {
                    pixelBuffer = await result.GetPixelsAsync();
                    savePicker.PickSaveFileAndContinue();
                }
                else
                    await errSave.ShowAsync();
            }
        }

        private void Adding_ManipulationStarted(object sender, ManipulationStartedRoutedEventArgs e)
        {
            rotation.CenterX = adding.PixelWidth / 2;
            rotation.CenterY = adding.PixelHeight / 2;
            resize.CenterX = adding.PixelWidth / 2;
            resize.CenterY = adding.PixelHeight / 2;
        }


        
    }
}
