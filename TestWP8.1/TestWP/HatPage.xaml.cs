﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Windows.UI.Xaml.Media.Imaging;
using TestWP.Common;
// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkID=390556

namespace TestWP
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class HatPage : Page
    {
        BitmapImage temp = new BitmapImage();
        public HatPage()
        {
            this.NavigationCacheMode = NavigationCacheMode.Required;
            this.InitializeComponent();
            Windows.Phone.UI.Input.HardwareButtons.BackPressed += HardwareButtons_BackPressed;
        }

        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.
        /// This parameter is typically used to configure the page.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
        }

         void HardwareButtons_BackPressed(object sender, Windows.Phone.UI.Input.BackPressedEventArgs e)
        {
            Frame.Navigate(typeof(Page1));
            e.Handled = true;
        }

        private void Hat1_Tapped(object sender, TappedRoutedEventArgs e)
        {
            temp = (BitmapImage)Hat1.Source;
            string path = temp.UriSource.ToString();
            Frame.Navigate(typeof(Page1), path);
        }

        private void Hat2_Tapped(object sender, TappedRoutedEventArgs e)
        {
            temp = (BitmapImage)Hat2.Source;
            string path = temp.UriSource.ToString();
            Frame.Navigate(typeof(Page1), path);
        }

        private void Hat3_Tapped(object sender, TappedRoutedEventArgs e)
        {
            temp = (BitmapImage)Hat3.Source;
            string path = temp.UriSource.ToString();
            Frame.Navigate(typeof(Page1), path);
        }

        private void Hat4_Tapped(object sender, TappedRoutedEventArgs e)
        {
            temp = (BitmapImage)Hat4.Source;
            string path = temp.UriSource.ToString();
            Frame.Navigate(typeof(Page1), path);
        }

        private void Hat5_Tapped(object sender, TappedRoutedEventArgs e)
        {
            temp = (BitmapImage)Hat5.Source;
            string path = temp.UriSource.ToString();
            Frame.Navigate(typeof(Page1), path);
        }

        private void Hat6_Tapped(object sender, TappedRoutedEventArgs e)
        {
            temp = (BitmapImage)Hat6.Source;
            string path = temp.UriSource.ToString();
            Frame.Navigate(typeof(Page1), path);
        }

        private void Hat7_Tapped(object sender, TappedRoutedEventArgs e)
        {
            temp = (BitmapImage)Hat7.Source;
            string path = temp.UriSource.ToString();
            Frame.Navigate(typeof(Page1), path);
        }

        private void Hat8_Tapped(object sender, TappedRoutedEventArgs e)
        {
            temp = (BitmapImage)Hat8.Source;
            string path = temp.UriSource.ToString();
            Frame.Navigate(typeof(Page1), path);
        }

        private void Hat9_Tapped(object sender, TappedRoutedEventArgs e)
        {
            temp = (BitmapImage)Hat9.Source;
            string path = temp.UriSource.ToString();
            Frame.Navigate(typeof(Page1), path);
        }

        private void Hat10_Tapped(object sender, TappedRoutedEventArgs e)
        {
            temp = (BitmapImage)Hat10.Source;
            string path = temp.UriSource.ToString();
            Frame.Navigate(typeof(Page1), path);
        }

        private void Hat11_Tapped(object sender, TappedRoutedEventArgs e)
        {
            temp = (BitmapImage)Hat11.Source;
            string path = temp.UriSource.ToString();
            Frame.Navigate(typeof(Page1), path);
        }

        private void Hat12_Tapped(object sender, TappedRoutedEventArgs e)
        {
            temp = (BitmapImage)Hat12.Source;
            string path = temp.UriSource.ToString();
            Frame.Navigate(typeof(Page1), path);
        }

        private void Hat13_Tapped(object sender, TappedRoutedEventArgs e)
        {
            temp = (BitmapImage)Hat13.Source;
            string path = temp.UriSource.ToString();
            Frame.Navigate(typeof(Page1), path);
        }

        private void Hat14_Tapped(object sender, TappedRoutedEventArgs e)
        {
            temp = (BitmapImage)Hat14.Source;
            string path = temp.UriSource.ToString();
            Frame.Navigate(typeof(Page1), path);
        }

        private void Hat15_Tapped(object sender, TappedRoutedEventArgs e)
        {
            temp = (BitmapImage)Hat15.Source;
            string path = temp.UriSource.ToString();
            Frame.Navigate(typeof(Page1), path);
        }

        private void Hat16_Tapped(object sender, TappedRoutedEventArgs e)
        {
            temp = (BitmapImage)Hat16.Source;
            string path = temp.UriSource.ToString();
            Frame.Navigate(typeof(Page1), path);
        }
    }
}
