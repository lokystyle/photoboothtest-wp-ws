﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Windows.UI.Xaml.Media.Imaging;
using TestWP.Common;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkID=390556

namespace TestWP
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class NosePage : Page
    {
        BitmapImage temp = new BitmapImage();
        public NosePage()
        {
            this.NavigationCacheMode = NavigationCacheMode.Required;
            this.InitializeComponent();
            Windows.Phone.UI.Input.HardwareButtons.BackPressed += HardwareButtons_BackPressed;
        }

        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.
        /// This parameter is typically used to configure the page.</param>
        void HardwareButtons_BackPressed(object sender, Windows.Phone.UI.Input.BackPressedEventArgs e)
        {
            Frame.Navigate(typeof(Page1));
            e.Handled = true;
        }
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
 
        }

        private void Nose1_Tapped(object sender, TappedRoutedEventArgs e)
        {
            temp = (BitmapImage)Nose1.Source;
            string path = temp.UriSource.ToString();
            Frame.Navigate(typeof(Page1), path);
        }

        private void Nose2_Tapped(object sender, TappedRoutedEventArgs e)
        {
            temp = (BitmapImage)Nose2.Source;
            string path = temp.UriSource.ToString();
            Frame.Navigate(typeof(Page1), path);
        }

        private void Nose3_Tapped(object sender, TappedRoutedEventArgs e)
        {
            temp = (BitmapImage)Nose3.Source;
            string path = temp.UriSource.ToString();
            Frame.Navigate(typeof(Page1), path);
        }

        private void Nose4_Tapped(object sender, TappedRoutedEventArgs e)
        {
            temp = (BitmapImage)Nose4.Source;
            string path = temp.UriSource.ToString();
            Frame.Navigate(typeof(Page1), path);
        }

        private void Nose5_Tapped(object sender, TappedRoutedEventArgs e)
        {
            temp = (BitmapImage)Nose5.Source;
            string path = temp.UriSource.ToString();
            Frame.Navigate(typeof(Page1), path);
        }

        private void Nose6_Tapped(object sender, TappedRoutedEventArgs e)
        {
            temp = (BitmapImage)Nose6.Source;
            string path = temp.UriSource.ToString();
            Frame.Navigate(typeof(Page1), path);
        }

        private void Nose7_Tapped(object sender, TappedRoutedEventArgs e)
        {
            temp = (BitmapImage)Nose7.Source;
            string path = temp.UriSource.ToString();
            Frame.Navigate(typeof(Page1), path);
        }

        private void Nose8_Tapped(object sender, TappedRoutedEventArgs e)
        {
            temp = (BitmapImage)Nose8.Source;
            string path = temp.UriSource.ToString();
            Frame.Navigate(typeof(Page1), path);
        }

        private void Nose9_Tapped(object sender, TappedRoutedEventArgs e)
        {
            temp = (BitmapImage)Nose9.Source;
            string path = temp.UriSource.ToString();
            Frame.Navigate(typeof(Page1), path);
        }

        private void Nose10_Tapped(object sender, TappedRoutedEventArgs e)
        {
            temp = (BitmapImage)Nose10.Source;
            string path = temp.UriSource.ToString();
            Frame.Navigate(typeof(Page1), path);
        }

        private void Nose11_Tapped(object sender, TappedRoutedEventArgs e)
        {
            temp = (BitmapImage)Nose11.Source;
            string path = temp.UriSource.ToString();
            Frame.Navigate(typeof(Page1), path);
        }

        private void Nose12_Tapped(object sender, TappedRoutedEventArgs e)
        {
            temp = (BitmapImage)Nose12.Source;
            string path = temp.UriSource.ToString();
            Frame.Navigate(typeof(Page1), path);
        }

        private void Nose13_Tapped(object sender, TappedRoutedEventArgs e)
        {
            temp = (BitmapImage)Nose13.Source;
            string path = temp.UriSource.ToString();
            Frame.Navigate(typeof(Page1), path);
        }

        private void Nose14_Tapped(object sender, TappedRoutedEventArgs e)
        {
            temp = (BitmapImage)Nose14.Source;
            string path = temp.UriSource.ToString();
            Frame.Navigate(typeof(Page1), path);
        }

        private void Nose15_Tapped(object sender, TappedRoutedEventArgs e)
        {
            temp = (BitmapImage)Nose15.Source;
            string path = temp.UriSource.ToString();
            Frame.Navigate(typeof(Page1), path);
        }

        private void Nose16_Tapped(object sender, TappedRoutedEventArgs e)
        {
            temp = (BitmapImage)Nose16.Source;
            string path = temp.UriSource.ToString();
            Frame.Navigate(typeof(Page1), path);
        }

        private void Nose17_Tapped(object sender, TappedRoutedEventArgs e)
        {
            temp = (BitmapImage)Nose17.Source;
            string path = temp.UriSource.ToString();
            Frame.Navigate(typeof(Page1), path);
        }

        private void Nose18_Tapped(object sender, TappedRoutedEventArgs e)
        {
            temp = (BitmapImage)Nose18.Source;
            string path = temp.UriSource.ToString();
            Frame.Navigate(typeof(Page1), path);
        }

        private void Nose19_Tapped(object sender, TappedRoutedEventArgs e)
        {
            temp = (BitmapImage)Nose19.Source;
            string path = temp.UriSource.ToString();
            Frame.Navigate(typeof(Page1), path);
        }

        private void Nose20_Tapped(object sender, TappedRoutedEventArgs e)
        {
            temp = (BitmapImage)Nose20.Source;
            string path = temp.UriSource.ToString();
            Frame.Navigate(typeof(Page1), path);
        }

        private void Nose21_Tapped(object sender, TappedRoutedEventArgs e)
        {
            temp = (BitmapImage)Nose21.Source;
            string path = temp.UriSource.ToString();
            Frame.Navigate(typeof(Page1), path);
        }
    }
}
