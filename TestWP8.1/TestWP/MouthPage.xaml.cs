﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Windows.UI.Xaml.Media.Imaging;
using TestWP.Common;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkID=390556

namespace TestWP
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MouthPage : Page
    {
        BitmapImage temp = new BitmapImage();
        public MouthPage()
        {
            this.NavigationCacheMode = NavigationCacheMode.Required;
            this.InitializeComponent();
            Windows.Phone.UI.Input.HardwareButtons.BackPressed += HardwareButtons_BackPressed;
        }

        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.
        /// This parameter is typically used to configure the page.</param>
        void HardwareButtons_BackPressed(object sender, Windows.Phone.UI.Input.BackPressedEventArgs e)
        {
            Frame.Navigate(typeof(Page1));
            e.Handled = true;
        }
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
           
        }

        private void Mouth1_Tapped(object sender, TappedRoutedEventArgs e)
        {
            temp = (BitmapImage)Mouth1.Source;
            string path = temp.UriSource.ToString();
            Frame.Navigate(typeof(Page1), path);
        }

        private void Mouth2_Tapped(object sender, TappedRoutedEventArgs e)
        {
            temp = (BitmapImage)Mouth2.Source;
            string path = temp.UriSource.ToString();
            Frame.Navigate(typeof(Page1), path);
        }

        private void Mouth3_Tapped(object sender, TappedRoutedEventArgs e)
        {
            temp = (BitmapImage)Mouth3.Source;
            string path = temp.UriSource.ToString();
            Frame.Navigate(typeof(Page1), path);
        }

        private void Mouth4_Tapped(object sender, TappedRoutedEventArgs e)
        {
            temp = (BitmapImage)Mouth4.Source;
            string path = temp.UriSource.ToString();
            Frame.Navigate(typeof(Page1), path);
        }

        private void Mouth5_Tapped(object sender, TappedRoutedEventArgs e)
        {
            temp = (BitmapImage)Mouth5.Source;
            string path = temp.UriSource.ToString();
            Frame.Navigate(typeof(Page1), path);
        }

        private void Mouth6_Tapped(object sender, TappedRoutedEventArgs e)
        {
            temp = (BitmapImage)Mouth6.Source;
            string path = temp.UriSource.ToString();
            Frame.Navigate(typeof(Page1), path);
        }

        private void Mouth7_Tapped(object sender, TappedRoutedEventArgs e)
        {
            temp = (BitmapImage)Mouth7.Source;
            string path = temp.UriSource.ToString();
            Frame.Navigate(typeof(Page1), path);
        }

        private void Mouth8_Tapped(object sender, TappedRoutedEventArgs e)
        {
            temp = (BitmapImage)Mouth8.Source;
            string path = temp.UriSource.ToString();
            Frame.Navigate(typeof(Page1), path);
        }

        private void Mouth9_Tapped(object sender, TappedRoutedEventArgs e)
        {
            temp = (BitmapImage)Mouth9.Source;
            string path = temp.UriSource.ToString();
            Frame.Navigate(typeof(Page1), path);
        }

        private void Mouth10_Tapped(object sender, TappedRoutedEventArgs e)
        {
            temp = (BitmapImage)Mouth10.Source;
            string path = temp.UriSource.ToString();
            Frame.Navigate(typeof(Page1), path);
        }

        private void Mouth11_Tapped(object sender, TappedRoutedEventArgs e)
        {
            temp = (BitmapImage)Mouth11.Source;
            string path = temp.UriSource.ToString();
            Frame.Navigate(typeof(Page1), path);
        }

        private void Mouth12_Tapped(object sender, TappedRoutedEventArgs e)
        {
            temp = (BitmapImage)Mouth12.Source;
            string path = temp.UriSource.ToString();
            Frame.Navigate(typeof(Page1), path);
        }

        private void Mouth13_Tapped(object sender, TappedRoutedEventArgs e)
        {
            temp = (BitmapImage)Mouth13.Source;
            string path = temp.UriSource.ToString();
            Frame.Navigate(typeof(Page1), path);
        }

        private void Mouth14_Tapped(object sender, TappedRoutedEventArgs e)
        {
            temp = (BitmapImage)Mouth14.Source;
            string path = temp.UriSource.ToString();
            Frame.Navigate(typeof(Page1), path);
        }

        private void Mouth15_Tapped(object sender, TappedRoutedEventArgs e)
        {
            temp = (BitmapImage)Mouth15.Source;
            string path = temp.UriSource.ToString();
            Frame.Navigate(typeof(Page1), path);
        }

        private void Mouth16_Tapped(object sender, TappedRoutedEventArgs e)
        {
            temp = (BitmapImage)Mouth16.Source;
            string path = temp.UriSource.ToString();
            Frame.Navigate(typeof(Page1), path);
        }

        private void Mouth17_Tapped(object sender, TappedRoutedEventArgs e)
        {
            temp = (BitmapImage)Mouth17.Source;
            string path = temp.UriSource.ToString();
            Frame.Navigate(typeof(Page1), path);
        }

        private void Mouth18_Tapped(object sender, TappedRoutedEventArgs e)
        {
            temp = (BitmapImage)Mouth18.Source;
            string path = temp.UriSource.ToString();
            Frame.Navigate(typeof(Page1), path);
        }

        private void Mouth19_Tapped(object sender, TappedRoutedEventArgs e)
        {
            temp = (BitmapImage)Mouth19.Source;
            string path = temp.UriSource.ToString();
            Frame.Navigate(typeof(Page1), path);
        }

        private void Mouth20_Tapped(object sender, TappedRoutedEventArgs e)
        {
            temp = (BitmapImage)Mouth20.Source;
            string path = temp.UriSource.ToString();
            Frame.Navigate(typeof(Page1), path);
        }

        private void Mouth21_Tapped(object sender, TappedRoutedEventArgs e)
        {
            temp = (BitmapImage)Mouth21.Source;
            string path = temp.UriSource.ToString();
            Frame.Navigate(typeof(Page1), path);
        }
    }
}
