﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Windows.UI.Xaml.Media.Imaging;
using TestWP.Common;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkID=390556

namespace TestWP
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class ScarsPage : Page
    {
        BitmapImage temp = new BitmapImage();
        public ScarsPage()
        {
            this.NavigationCacheMode = NavigationCacheMode.Required;
            this.InitializeComponent();
            Windows.Phone.UI.Input.HardwareButtons.BackPressed += HardwareButtons_BackPressed;
        }

        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.
        /// This parameter is typically used to configure the page.</param>
        void HardwareButtons_BackPressed(object sender, Windows.Phone.UI.Input.BackPressedEventArgs e)
        {
            Frame.Navigate(typeof(Page1));
            e.Handled = true;
        }
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
          
        }

        private void Scars1_Tapped(object sender, TappedRoutedEventArgs e)
        {
            temp = (BitmapImage)Scars1.Source;
            string path = temp.UriSource.ToString();
            Frame.Navigate(typeof(Page1), path);
        }

        private void Scars2_Tapped(object sender, TappedRoutedEventArgs e)
        {
            temp = (BitmapImage)Scars2.Source;
            string path = temp.UriSource.ToString();
            Frame.Navigate(typeof(Page1), path);
        }

        private void Scars3_Tapped(object sender, TappedRoutedEventArgs e)
        {
            temp = (BitmapImage)Scars3.Source;
            string path = temp.UriSource.ToString();
            Frame.Navigate(typeof(Page1), path);
        }

        private void Scars4_Tapped(object sender, TappedRoutedEventArgs e)
        {
            temp = (BitmapImage)Scars4.Source;
            string path = temp.UriSource.ToString();
            Frame.Navigate(typeof(Page1), path);
        }

        private void Scars5_Tapped(object sender, TappedRoutedEventArgs e)
        {
            temp = (BitmapImage)Scars5.Source;
            string path = temp.UriSource.ToString();
            Frame.Navigate(typeof(Page1), path);
        }

        private void Scars6_Tapped(object sender, TappedRoutedEventArgs e)
        {
            temp = (BitmapImage)Scars6.Source;
            string path = temp.UriSource.ToString();
            Frame.Navigate(typeof(Page1), path);
        }

        private void Scars7_Tapped(object sender, TappedRoutedEventArgs e)
        {
            temp = (BitmapImage)Scars7.Source;
            string path = temp.UriSource.ToString();
            Frame.Navigate(typeof(Page1), path);
        }

        private void Scars8_Tapped(object sender, TappedRoutedEventArgs e)
        {
            temp = (BitmapImage)Scars8.Source;
            string path = temp.UriSource.ToString();
            Frame.Navigate(typeof(Page1), path);
        }

        private void Scars9_Tapped(object sender, TappedRoutedEventArgs e)
        {
            temp = (BitmapImage)Scars9.Source;
            string path = temp.UriSource.ToString();
            Frame.Navigate(typeof(Page1), path);
        }

        private void Scars10_Tapped(object sender, TappedRoutedEventArgs e)
        {
            temp = (BitmapImage)Scars10.Source;
            string path = temp.UriSource.ToString();
            Frame.Navigate(typeof(Page1), path);
        }

        private void Scars11_Tapped(object sender, TappedRoutedEventArgs e)
        {
            temp = (BitmapImage)Scars11.Source;
            string path = temp.UriSource.ToString();
            Frame.Navigate(typeof(Page1), path);
        }

        private void Scars12_Tapped(object sender, TappedRoutedEventArgs e)
        {
            temp = (BitmapImage)Scars12.Source;
            string path = temp.UriSource.ToString();
            Frame.Navigate(typeof(Page1), path);
        }

        private void Scars13_Tapped(object sender, TappedRoutedEventArgs e)
        {
            temp = (BitmapImage)Scars13.Source;
            string path = temp.UriSource.ToString();
            Frame.Navigate(typeof(Page1), path);
        }

        private void Scars14_Tapped(object sender, TappedRoutedEventArgs e)
        {
            temp = (BitmapImage)Scars14.Source;
            string path = temp.UriSource.ToString();
            Frame.Navigate(typeof(Page1), path);
        }

        private void Scars15_Tapped(object sender, TappedRoutedEventArgs e)
        {
            temp = (BitmapImage)Scars15.Source;
            string path = temp.UriSource.ToString();
            Frame.Navigate(typeof(Page1), path);
        }

        private void Scars16_Tapped(object sender, TappedRoutedEventArgs e)
        {
            temp = (BitmapImage)Scars16.Source;
            string path = temp.UriSource.ToString();
            Frame.Navigate(typeof(Page1), path);
        }

        private void Scars17_Tapped(object sender, TappedRoutedEventArgs e)
        {
            temp = (BitmapImage)Scars17.Source;
            string path = temp.UriSource.ToString();
            Frame.Navigate(typeof(Page1), path);
        }

        private void Scars18_Tapped(object sender, TappedRoutedEventArgs e)
        {
            temp = (BitmapImage)Scars18.Source;
            string path = temp.UriSource.ToString();
            Frame.Navigate(typeof(Page1), path);
        }

        private void Scars19_Tapped(object sender, TappedRoutedEventArgs e)
        {
            temp = (BitmapImage)Scars19.Source;
            string path = temp.UriSource.ToString();
            Frame.Navigate(typeof(Page1), path);
        }

        private void Scars20_Tapped(object sender, TappedRoutedEventArgs e)
        {
            temp = (BitmapImage)Scars20.Source;
            string path = temp.UriSource.ToString();
            Frame.Navigate(typeof(Page1), path);
        }

    }
}
